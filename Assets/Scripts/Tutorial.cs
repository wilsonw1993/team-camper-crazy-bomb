﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour
{
    public Canvas panelInfoCanvas;
    //public GameObject panelInfoCanvas;
	public GameObject optionsTint;
    public bool infoOpen = false;

	// Use this for initialization
	public void InfoPanel()
    {
        if (infoOpen == false)
        {
            infoOpen = true;
            panelInfoCanvas.enabled = true;
			optionsTint.SetActive(true);
        }
        else if (infoOpen == true)
        {
            infoOpen = false;
            panelInfoCanvas.enabled = false;
			optionsTint.SetActive(false);
        }
    }

    public void back()
    {
        infoOpen = false;
        panelInfoCanvas.enabled = false;
		optionsTint.SetActive(false);
    }
}
