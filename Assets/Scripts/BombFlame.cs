﻿using UnityEngine;
using System.Collections;

public class BombFlame : MonoBehaviour
{

    public float timer;
    private Player player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player1").GetComponent<Player>();
        Destroy(gameObject, timer);
    }

    //detects when collisions with the flame with player and proceeds with killing the player
    public void OnTriggerEnter2D(Collider2D col)
    {
        player.Damage(100);

    }
}
