
﻿using UnityEngine;
using System.Collections;


public class Item : MonoBehaviour {

    private Player player;
	public GPSManager manager;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player1").GetComponent<Player>();
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Collider object " + col.tag);
        Debug.Log("potion object " + gameObject.tag);

        switch (gameObject.tag)
        {
            case "Potion_b": // bomber's power
                player.BombPowerUp();
				manager.BluePotionAch();
                Debug.Log("potion object : potion_blue");
                break;
            case "Potion_r": // player's speed 
                player.SpeedUp();
				manager.RedPotionAch();
                Debug.Log("potion object : potion_red");
                break;
            case "Potion_y": // bomber Up
                player.addBomb(3);
				manager.YellowPotionAch();
                Debug.Log("potion object : potion_yellow");
                break;
        }
        Destroy(gameObject);
        Debug.Log("potion gone");
    }
}

