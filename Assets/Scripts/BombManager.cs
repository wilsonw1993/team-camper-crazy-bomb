﻿using UnityEngine;
using System.Collections;

public class BombManager : MonoBehaviour{
	
	private bool top_object;
	private bool bot_object;
	private bool right_object;
	private bool left_object;
	
	private bool box_up;
	private bool box_down;
	private bool box_left;
	private bool box_right;
	
	private float x;
	private float y;
	private float width;
	private float height;
	
	public GameObject flame_4way;
	public GameObject flame_2way_hor;
	public GameObject flame_2way_ver;
	public GameObject flame_tip_top;
	public GameObject flame_tip_bot;
	public GameObject flame_tip_left;
	public GameObject flame_tip_right;
	
    public GameObject potion_red;       // player's speed 
	public GameObject potion_blue;      // bomber's power
	public GameObject potion_yellow;    // bomber up


    public void Set(Transform bombTransform) {
        x = bombTransform.position.x;
        y = bombTransform.position.y;

        height = bombTransform.GetComponent<Renderer>().bounds.size.y;
        width = bombTransform.GetComponent<Renderer>().bounds.size.x;
        
    }

	
	//Checks if the flame collides with any object and sets appropriate bools to determine use in flame making and box destruction
	public void CollisionDetector() {
		var left = Physics2D.OverlapPoint(new Vector2 (x-width, y));
		var right = Physics2D.OverlapPoint(new Vector2(x+width, y));                           
		var top = Physics2D.OverlapPoint(new Vector2(x, y+height));
		var bottom = Physics2D.OverlapPoint(new Vector2(x, y-height));
		
		// check top
		if (top && top.CompareTag("Brick")) {
			top_object = true;
		} 
		else if (top && top.CompareTag("Box")) {
			DestroyBox();
			box_up = true;
		}
		else {
			top_object = false;
		}
		
		// check bottom
		if (bottom && bottom.CompareTag("Brick")) {
			bot_object = true;
		}
		else if (bottom && bottom.CompareTag("Box")) {
			DestroyBox();
			box_down = true;
		}
		else {
			bot_object = false;
		}
		
		// check left
		if (left && left.CompareTag("Brick")) {
			left_object = true;
		}
		else if (left && left.CompareTag("Box")) {
			DestroyBox();
			box_left = true;
		}
		else {
			left_object = false;
		}
		
		// check right
		if (right && right.CompareTag("Brick")) {
			right_object = true;
		}
		else if (right && right.CompareTag("Box")) {
			DestroyBox();
			box_right = true;
		}
		else {
			right_object = false;
		}
	}
	
	//Needed a bigger threshold for approximate coords than the built-in Approx function. 0.2 seemed enough 
	public bool Approx(float x, float y) {
		return (Mathf.Abs (x - y) < 0.2);
	}
	
	//If the box location is next to where a bomb was placed (x,y), then the box will be destroyed
	public void DestroyBox() {
		GameObject[] Boxes = GameObject.FindGameObjectsWithTag ("Box");
		foreach (GameObject thebox in Boxes) {
			bool proximity_x = Approx(thebox.transform.position.x, x);
			bool proximity_y = Approx(thebox.transform.position.y, y);
			float box_x = thebox.transform.position.x - x;
			float box_y = thebox.transform.position.y - y;
			if (proximity_x && (Mathf.Abs(box_y) <= 1.2) || proximity_y && (Mathf.Abs(box_x) <= 1.2)) {
				MakePotion(thebox);
				Destroy(thebox);
			}
		}
	}
	
	//Makes the flame depending on the directions needed
	public void MakeFlame (int flamePower, float step_x, float step_y, GameObject FlamePrefab, GameObject FlameTipPrefab) { //add string tag to arg???
		//Temp coords for flame building 
		float dir_x = width * step_x;
		float dir_y = height * step_y;
		float temp_x = x + dir_x;
		float temp_y = y + dir_y;
		
		for (int i = 0; i < flamePower; i++) {
			if (i + 1 == flamePower) {
				Instantiate (FlameTipPrefab, new Vector2 (temp_x, temp_y), Quaternion.identity);
			} 
			else if (!Physics2D.Linecast (new Vector2 (temp_x, temp_y), new Vector2 (temp_x + dir_x, temp_y + dir_y))) {
				Instantiate (FlamePrefab, new Vector2 (temp_x, temp_y), Quaternion.identity);
			} 
			else {
				Instantiate (FlameTipPrefab, new Vector2 (temp_x, temp_y), Quaternion.identity);
				break;
			}
			temp_x += dir_x;
			temp_y += dir_y;
		}
	}
	
	//Makes the middle part of the flame depending on the directions of the other flames 
	public void MakeMiddleFlame(Vector3 position) {
		if (top_object && bot_object && !left_object && !right_object) {
			Instantiate(flame_2way_hor, position, Quaternion.identity);
		}
		else if (!top_object && !bot_object && left_object && right_object) {
			Instantiate(flame_2way_ver, position, Quaternion.identity);
		}
		else {
			Instantiate(flame_4way, position, Quaternion.identity);
		}
	}
		void MakePotion(GameObject potion) {
		int type = (int)Random.Range(0, 12);
		
		Vector3 potionPost = new Vector3(potion.transform.position.x, potion.transform.position.y, potion.transform.position.z);
		switch (type)
		{
		case 0:
			break;
		case 1:
			Instantiate(potion_red, potionPost, Quaternion.identity);
			Debug.Log("red portion create");
			break;
		case 2:
			Instantiate(potion_blue, potionPost, Quaternion.identity);
			Debug.Log("blue portion create");
			break;
		case 3:
			Instantiate(potion_yellow, potionPost, Quaternion.identity);
			Debug.Log("yellow portion create");
			break;
		}
	}
	
	public bool HasTopObject() {
		return top_object;
	}
	public bool HasBottomObject() {
		return bot_object;
	}
	public bool HasLeftObject() {
		return left_object;
	}
	public bool HasRightObject() {
		return right_object;
	}
	public bool BoxTop() {
		return box_up;
	}
	public bool BoxDown() {
		return box_down;
	}
	public bool BoxLeft() {
		return box_left;
	}
	public bool BoxRight() {
		return box_right;
	}
}
