﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {

	public float bombTimer;
	public float flameTimer;
	public int flamePower;
	
	private Animator anim;
	private GameObject flame;
	private Player player;
	private BombManager manager;
	private int maxFlamePower = 8;
	
	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator>();
		// **need changes** ???? 
		player = GameObject.FindGameObjectWithTag("Player1").transform.GetComponent<Player>();
		manager = transform.GetComponent<BombManager>();
		manager.Set(transform);
		StartCoroutine(CountDown());
		
		int bombPower = player.GetBombPower();
		Debug.Log("flamePower : " + bombPower);
		
		if (bombPower > maxFlamePower)
			flamePower = maxFlamePower;
		else
			flamePower = bombPower;

        // switch on the trigger on collider2D 
        gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
	}
	
	//Countdown for the bomb to explode 
	public IEnumerator CountDown() {
		anim.SetFloat("Timer", bombTimer);
		yield return new WaitForSeconds(bombTimer);
		Explode();
	}
	
	//Destroys the bomb, initiates the flame animation and then reloads the player's bomb dropping ability 
	private void Explode() {     
		Destroy(gameObject);     
		Flame();
		player.recoverBomb();
	}
	
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player1")==true) {
            player.canPlant = false;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Player1") == true) {
            player.canPlant = true;

            // if changing the trigger to false, the player can't walk pass the bomb
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }

	private void Flame() {
		//Checks the collision for the middle tile and then makes the middle tile flame
		manager.CollisionDetector();
		manager.MakeMiddleFlame(transform.position);
		
		//Each conditional handles the flame creation for the corresponding direction
		
		// top
		if (!manager.HasTopObject())
		{
			manager.MakeFlame(flamePower, 0, 1, manager.flame_2way_ver, manager.flame_tip_top);
		}
		
		// bottom
		if (!manager.HasBottomObject())
		{
			manager.MakeFlame(flamePower, 0, -1, manager.flame_2way_ver, manager.flame_tip_bot);
		}
		
		// right
		if (!manager.HasRightObject())
		{
			manager.MakeFlame(flamePower, 1, 0, manager.flame_2way_hor, manager.flame_tip_right);
		}
		
		// left
		if (!manager.HasLeftObject())
		{
			manager.MakeFlame(flamePower, -1, 0, manager.flame_2way_hor, manager.flame_tip_left);
		}
	}
}
