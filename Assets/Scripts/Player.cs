﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	public float speed;
	public float maxSpeed = 0.2f;
	public int bombTotal;
	public int bombLeft;
	
	private bool faceRight;
	private bool faceLeft;
	private bool faceUp;
	private bool faceDown;
	private bool walking;
	private int flamePower;
	
	public KeyCode moveUp;
	public KeyCode moveDown;
	public KeyCode moveRight;
	public KeyCode moveLeft;
	public KeyCode plantBomb;
	
	private Rigidbody2D rb2d;
	private Animator anim;
	private Transform trans;
	private Vector3 position;
	
	public GameObject bomb;
	public GameObject tombstone;
	
	bool MoveUp = false;
	bool MoveDown = false;
	bool MoveLeft = false;
	bool MoveRight = false;
	bool Action = false;
	
	public int curHealth;
	public int maxHealth = 100;
    public bool canPlant = true;

	public GPSManager manager;
	
	// Use this for initialization
	void Start() {
		rb2d = gameObject.GetComponent<Rigidbody2D>();
		anim = gameObject.GetComponent<Animator>();
		bombLeft = bombTotal;
		position = transform.position;
		rb2d.gravityScale = 0;
		curHealth = maxHealth;
		flamePower = 1;
		manager.PlayCounterAch();
	}
	
	public void OnDownMoveUp() {
		MoveUp = true;
	}
	
	public void OnDownMoveDown() {
		MoveDown = true;
	}
	
	public void OnDownMoveLeft() {
		MoveLeft = true;
	}
	
	public void OnDownMoveRight() {
		MoveRight = true;
	}
	
	public void OnDownAction() {
		Action = true;
	}
	
	public void OnUpMoveUp() {
		MoveUp = false;
	}
	
	public void OnUpMoveDown() {
		MoveDown = false;
	}
	
	public void OnUpMoveLeft() {
		MoveLeft = false;
	}
	
	public void OnUpMoveRight() {
		MoveRight = false;
	}
	
	public void OnUpAction() {
		Action = false;
	}
	
	void Update() {
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");
		anim.SetFloat("Speed", Mathf.Abs(Mathf.Sqrt(x*x + y*y)));
		anim.SetBool("FaceRight", faceRight);
		anim.SetBool("FaceUp", faceUp);
		anim.SetBool("FaceDown", faceDown);
		anim.SetBool("FaceLeft", faceLeft);
		anim.SetBool("Walk", walking);
		
		//Moves the player
		if (transform.position == position) {
			walking = false;
			if (MoveRight == true || x > 0) {           
				position += Vector3.right;
				walking = true;
				faceRight = true;
				faceLeft = faceUp = faceDown = false;
				if (!Walkable(position))
					position = transform.position;
			}
			if (MoveLeft == true || x < 0) {
				position += Vector3.left;
				walking = true;
				faceLeft = true;
				faceRight = faceUp = faceDown = false;
				if (!Walkable(position))
					position = transform.position;
			}
			if (MoveUp == true || y > 0) {
				position += Vector3.up;
				walking = true;
				faceUp = true;
				faceRight = faceLeft = faceDown = false;
				if (!Walkable(position))
					position = transform.position;
			}
			if (MoveDown == true || y < 0) {
				position += Vector3.down;
				walking = true;
				faceDown = true;
				faceRight = faceLeft = faceUp = false;
				if (!Walkable(position)) {
					position = transform.position;
				}
			}
		}
		
		//Places bomb on map when triggered by Action key 
		if (Input.GetKey(plantBomb) && bombLeft > 0 && canPlant || Action && bombLeft > 0 && canPlant) {
			bombLeft -= 1;
			PlantBomb();
		}
		
		//Handles player moving in tiles     
		transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * speed);
	}
	
	void PlantBomb() {
		Vector3 bombPost = new Vector3(transform.position.x, transform.position.y, transform.position.z);
		
		//bombInstance.transform.SetParent(transform, false);
		Instantiate(bomb, bombPost, Quaternion.identity);
		manager.BombCounterAch();
		manager.ManiacTwoCounterAch();
	}
	
	//Resetting the number of bombs the player has
	public void recoverBomb()
	{
		bombLeft += 1;
		Debug.Log("Bomber Count: " + bombLeft);
	}
	
	public void addBomb(int cnt)
	{
		bombLeft += cnt;
		Debug.Log("Bomber Count: " + bombLeft);
	}
	
	public void BombPowerUp()
	{
		flamePower += 1;
		Debug.Log("Bomber Power: " + flamePower);
	}
	
	public int GetBombPower()
	{
		return flamePower;
	}
	
	public void SpeedUp()
	{
		if (speed < maxSpeed)
			speed += 1;
		Debug.Log("Speed Up: " + speed);
	}
	
	
	public bool Walkable(Vector3 attempt) {
		// If detect game object, don't move
		var target = Physics2D.OverlapPoint (new Vector2 (attempt.x, attempt.y));
		if (target != null && target.CompareTag("Brick")) {
			//if ((Physics2D.OverlapPoint(new Vector2 (attempt.x, attempt.y))).CompareTag("Brick")) {
			return false;
		}
		else if (target != null && target.CompareTag("Box"))
		{
			//if ((Physics2D.OverlapPoint(new Vector2 (attempt.x, attempt.y))).CompareTag("Brick")) {
			return false;
		} 
        else if (target != null && target.CompareTag("Bomb")) {
            return false;
        }
        else if (target != null && target.CompareTag("Tomb")) {
            return false;
        }
		else {
			return true;
		}
	}
	
	//code to destroy the player unit upon impact with flame
	//& also spawns gravestone to indicate player's death
	public void Damage(int dmg) {
		curHealth -= dmg;
		Destroy(gameObject);
		Instantiate(tombstone, transform.position, transform.rotation);
		Debug.Log("current health is " + curHealth);
		manager.DeathCounterAch();
	}
}
