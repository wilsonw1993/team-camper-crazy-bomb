﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BoxSpawner : NetworkBehaviour {

    public GameObject BoxPrefab;

    public int boxTotal = 20;

    public float[] possibleX;
    public float[] nonConstraintY;
    public float[] constraintY;

    void Awake()
    {
        possibleX = new float[11] { -4.5f, -3.5f, -2.5f, -1.5f, -0.5f, 0.5f,
                                    1.5f, 2.5f, 3.5f, 4.5f, 5.5f };

        nonConstraintY = new float[11] { -4.5f, -3.5f, -2.5f, -1.5f, -0.5f, 0.5f,
                                    1.5f, 2.5f, 3.5f, 4.5f, 5.5f };

        // places that has brick, on y...
        constraintY = new float[6] { 5.5f, 3.5f, 1.5f, -0.5f, -2.5f, -4.5f };
    }

    public override void OnStartServer()
    {
        for (int i=0; i<boxTotal; i++) {

            // randomly choose an x value from the x array
            int x_index = Random.Range(0, possibleX.Length);
            int y_index;
            Debug.Log("x_index" + x_index);
            float x = possibleX[x_index];
            float y;

            // can't allow box to be spawn on player spawning spot
            if (x_index==0 || x_index==possibleX.Length) {
                y_index = Random.Range(2, possibleX.Length-2);
                y = nonConstraintY[y_index];
            }
            else if (x_index == 1 || x_index == possibleX.Length-1) {
                y_index = Random.Range(1, constraintY.Length-1);
                y = constraintY[y_index];
            }
            else if (x_index % 2 != 0) {
                y_index = Random.Range(0, constraintY.Length);
                y = constraintY[y_index];
            }
            else {
                y_index = Random.Range(0, possibleX.Length);
                y = nonConstraintY[y_index];
            }

            var box = (GameObject)Instantiate(BoxPrefab, new Vector2(x, y), Quaternion.identity);

            NetworkServer.Spawn(box);

        }
    }
}
