﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerTouchControlHook : MonoBehaviour {

    public delegate void TouchEvent();

    public TouchEvent OnDownMoveUp;
    public TouchEvent OnDownMoveDown;
    public TouchEvent OnDownMoveRight;
    public TouchEvent OnDownMoveLeft;
    public TouchEvent OnDownAction;
    public TouchEvent OnUpMoveDown;
    public TouchEvent OnUpMoveUp;
    public TouchEvent OnUpMoveRight;
    public TouchEvent OnUpMoveLeft;
    public TouchEvent OnUpAction;


    public Button upButton;
    public Button downButton;
    public Button rightButton;
    public Button leftButton;
    public Button actionButton;
    public RectTransform panelPos;

    public void SetLocalPlayer()
    {

        upButton.gameObject.SetActive(true);
        downButton.gameObject.SetActive(true);
        rightButton.gameObject.SetActive(true);
        leftButton.gameObject.SetActive(true);
        actionButton.gameObject.SetActive(true);
    }

    public void UIOnUpMoveUp()
    {
        if (OnUpMoveUp != null)
            OnUpMoveUp.Invoke();
    }

    public void UIOnUpMoveDown()
    {
        if (OnUpMoveDown != null)
            OnUpMoveDown.Invoke();
    }

    public void UIOnUpMoveLeft()
    {
        if (OnUpMoveLeft != null)
            OnUpMoveLeft.Invoke();
    }

    public void UIOnUpMoveRight()
    {
        if (OnUpMoveRight != null)
            OnUpMoveRight.Invoke();
    }

    public void UIOnUpAction()
    {
        if (OnUpAction != null)
            OnUpAction.Invoke();
    }

    public void UIOnDownMoveUp()
    {
        if (OnDownMoveUp != null)
            OnDownMoveUp.Invoke();
    }

    public void UIOnDownMoveDown()
    {
        if (OnDownMoveDown != null)
            OnDownMoveDown.Invoke();
    }

    public void UIOnDownMoveLeft()
    {
        if (OnDownMoveLeft != null)
            OnDownMoveLeft.Invoke();
    }

    public void UIOnDownMoveRight()
    {
        if (OnDownMoveRight != null)
            OnDownMoveRight.Invoke();
    }

    public void UIOnDownAction()
    {
        if (OnDownAction != null)
            OnDownAction.Invoke();
    }
}
