using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GPSManager : MonoBehaviour {

	// Use this for initialization
	public void Start ()
	{
        //lets you sign into google play service with your gmail account
	    PlayGamesPlatform.Activate();
	    Social.localUser.Authenticate((bool success) =>
	    {
	        if (success)
	        {
	            Debug.Log("Authentication Successful");
	        }
	        else
	        {
	            Debug.Log("Authentication Failed");
	        }
	    });
	}
    //displays player's achievements when button is clicked on homescreen
    public void ShowAchievements()
    {
        Social.ShowAchievementsUI();
    }

	public void BombCounterAch() {
		//Planter achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQAg", 1, (bool success) => {
			if (success) { 
				Debug.Log ("Bomb Counter Incremented");
			}
			else {
				Debug.Log ("disapOINTEDDDD"); 
			}
		});
		//BombCounterChecker();
	}
	public void BombCounterChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQAg", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Bomb Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
	public void PlayCounterAch(){
		//Good Start achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQAQ", 1, (bool success) => {
			if (success) {
				Debug.Log ("Play Counter Incremented");				
			}
			else {
				Debug.Log ("disapOINTEDDDD Y U NO PLAY"); 
			}
		});
		//PlayCounterChecker();
	}
	public void PlayCounterChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQAQ", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Play Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
	public void DeathCounterAch() {
		//Learn to play achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQBA", 1, (bool success) => {
			if (success) {
				Debug.Log ("Death Counter Incremented");				
			}
			else {
				Debug.Log ("I mean, least it didn't count"); 
			}
		});
		//DeathCounterChecker();
	}
	public void DeathCounterChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQBA", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Death Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
	public void ManiacCounterAch() {
		//Maniac achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQCQ", 1, (bool success) => {
			if (success) {
				Debug.Log ("Maniac Counter Incremented");				
			}
			else {
				Debug.Log ("I mean, least it didn't count"); 
			}
		});
		//ManiacCounterChecker();
	}
	public void ManiacCounterChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQCQ", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Maniac Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
	public void ManiacTwoCounterAch() {
		//Maniac 2.0 achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQCg", 1, (bool success) => {
			if (success) {
				Debug.Log ("Maniac 2 Counter Incremented");				
			}
			else {
				Debug.Log ("I mean, least it didn't count"); 
			}
		});
		//ManiacTwoCounterChecker();
	}
	public void ManiacTwoCounterChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQCg", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Maniac 2 Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
	public void RedPotionAch() {
		//Speed lover achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQBQ", 1, (bool success) => {
			if (success) {
				Debug.Log ("Red Potion Counter Incremented");				
			}
			else {
				Debug.Log ("I mean, least it didn't count"); 
			}
		});
		//RedPotionChecker();
	}
	public void RedPotionChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQBQ", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Red Potion Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
	public void BluePotionAch() {
		//UNLESS YOU'VE GOT POWER achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQBw", 1, (bool success) => {
			if (success) {
				Debug.Log ("Blue Potion Counter Incremented");				
			}
			else {
				Debug.Log ("I mean, least it didn't count"); 
			}
		});
		//BluePotionChecker();
	}
	public void BluePotionChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQBw", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Blue Potion Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
	public void YellowPotionAch() {
		//UNLESS YOU'VE GOT POWER achievement
		PlayGamesPlatform.Instance.IncrementAchievement("CgkIlquD3KQTEAIQCA", 1, (bool success) => {
			if (success) {
				Debug.Log ("Yellow Potion Counter Incremented");				
			}
			else {
				Debug.Log ("I mean, least it didn't count"); 
			}
		});
		//YellowPotionChecker();
	}
	public void YellowPotionChecker() {
		Social.ReportProgress ("CgkIlquD3KQTEAIQCA", 100.0f, (bool success) => {
			if (success) {
				Debug.Log ("Yellow Potion Counter Achieved");
			} 
			else {
				Debug.Log ("Nope"); 
			}
		});
	}
}
