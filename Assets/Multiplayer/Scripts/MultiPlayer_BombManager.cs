﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MultiPlayer_BombManager : NetworkBehaviour
{

    private bool top_object;
    private bool bot_object;
    private bool right_object;
    private bool left_object;

    private float x;
    private float y;
    private float width;
    private float height;

    public GameObject flame_4way;
    public GameObject flame_2way_hor;
    public GameObject flame_2way_ver;
    public GameObject flame_tip_top;
    public GameObject flame_tip_bot;
    public GameObject flame_tip_left;
    public GameObject flame_tip_right;

    public void Set(Transform bombTransform)
    {
        x = bombTransform.position.x;
        y = bombTransform.position.y;

        height = bombTransform.GetComponent<Renderer>().bounds.size.y;
        width = bombTransform.GetComponent<Renderer>().bounds.size.x;
        //Debug.Log("height: " + height + " width: " + width);
    }

    //Checks if the flame collides with any object

    //DONT GET RID OF THE COMMENTS IN THIS FUNCTION 

    public void CollisionDetector()
    {

        var left = Physics2D.OverlapPoint(new Vector2(x - width, y));
        var right = Physics2D.OverlapPoint(new Vector2(x + width, y));
        var top = Physics2D.OverlapPoint(new Vector2(x, y + height));
        var bottom = Physics2D.OverlapPoint(new Vector2(x, y - height));

        // check top
        if (top && top.CompareTag("Brick"))
        {
            top_object = true;
        }
        else if (top && top.CompareTag("Box"))
        {
            //box breaking stuff here
            //box.BoxDestroy(100);
        }/*
		if (top && top.CompareTag("Player1")) {
			
		} */

        else
        {
            top_object = false;
        }

        // check bottom
        if (bottom && bottom.CompareTag("Brick"))
        {
            bot_object = true;
        }

        else if (bottom && bottom.CompareTag("Box"))
        {
            //box breaking stuff here
            //box.BoxDestroy(100);
        }
        /*
        if (bottom && bottom.CompareTag("Player1")) {
			
        } 
        */
        else
        {
            bot_object = false;
        }

        // check left
        if (left && left.CompareTag ("Brick")) {
			left_object = true;
		} else if (left && left.CompareTag ("Box")) {
			//box breaking stuff here
		}
        /*
        if (left && left.CompareTag("Player1")) {
		//
        }
        */
        else
        {
            left_object = false;
        }

        // check right
        if (right && right.CompareTag("Brick"))
        {
            right_object = true;
        }

        else if (right && right.CompareTag("Box"))
        {
            //box breaking stuff here
        }
        /*
		if (right && right.CompareTag("Player1")) {
		
		}
		*/
        else
        {
            right_object = false;
        }
    }

    //could be useful, might not
	public bool ObjectDetector(Vector2 coord, string tag) {
		var target = Physics2D.OverlapPoint (new Vector2 (coord.x, coord.y));
		if (target != null && target.CompareTag(tag)) {
			return true;
		}
		return false;
	}

    public void MakeFlame(int flamePower, float step_x, float step_y, GameObject FlamePrefab, GameObject FlameTipPrefab)
    { //add string tag to arg???
      //Temp coords for flame building in y 
        float dir_x = width * step_x;
        float dir_y = height * step_y;
        float temp_x = x + dir_x;
        float temp_y = y + dir_y;

        for (int i = 0; i < flamePower; i++)
        {
            if (i + 1 == flamePower)
            {
                //Instantiate(FlameTipPrefab, new Vector2(temp_x, temp_y), Quaternion.identity);
                CreateFlame(FlameTipPrefab, new Vector2(temp_x, temp_y));
            }
            else if (ObjectDetector(new Vector2(temp_x + dir_x, temp_y + dir_y), "Brick"))
            {
                CreateFlame(FlameTipPrefab, new Vector2(temp_x, temp_y));
                break;
            }
            //else if (!Physics2D.Linecast(new Vector2(temp_x, temp_y), new Vector2(temp_x + dir_x, temp_y + dir_y)))
            else if(ObjectDetector(new Vector2(temp_x, temp_y), "Player"))
            { //change to objectdetector??? yes no?
              //Instantiate(FlamePrefab, new Vector2(temp_x, temp_y), Quaternion.identity);
                CreateFlame(FlamePrefab, new Vector2(temp_x, temp_y));
            }
            else if (ObjectDetector(new Vector2(temp_x, temp_y), "Box"))
            {
                //Instantiate(FlameTipPrefab, new Vector2(temp_x, temp_y), Quaternion.identity);
                CreateFlame(FlameTipPrefab, new Vector2(temp_x, temp_y));
                break;
            }//
            else
            {
                CreateFlame(FlamePrefab, new Vector2(temp_x, temp_y));
            }
            temp_x += dir_x;
            temp_y += dir_y;
        }
    }

    public void MakeMiddleFlame(Vector3 position)
    {
        if (top_object && bot_object && !left_object && !right_object)
        {
            //Instantiate(flame_2way_hor, position, Quaternion.identity);
            CreateFlame(flame_2way_hor, position);
        }
        else if (!top_object && !bot_object && left_object && right_object)
        {
            //Instantiate(flame_2way_ver, position, Quaternion.identity);
            CreateFlame(flame_2way_ver, position);
        }
        else
        {
            //Instantiate(flame_4way, position, Quaternion.identity);
            CreateFlame(flame_4way, position);
        }
    }

    public bool HasTopObject()
    {
        return top_object;
    }

    public bool HasBottomObject()
    {
        return bot_object;
    }

    public bool HasLeftObject()
    {
        return left_object;
    }

    public bool HasRightObject()
    {
        return right_object;
    }
    [Command]
    private void CmdCreateFlame(GameObject prefab, Vector2 position)
    {
        GameObject flame = Instantiate(prefab, position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(flame);
    }

    private void CreateFlame(GameObject prefab, Vector2 position)
    {
        GameObject flame = Instantiate(prefab, position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(flame);
    }
}
