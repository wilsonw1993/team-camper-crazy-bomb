﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MultiPlayer_BombFlame : NetworkBehaviour
{

    public float timer;

	// Use this for initialization
	void Start () {
        StartCoroutine(Flaming());
    }


    public IEnumerator Flaming()
    {
        yield return new WaitForSeconds(timer);
        Destroy(gameObject);
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player") == true) {
            col.gameObject.GetComponent<MultiPlayer_Player>().TakeDamage(100);
        }
        if (col.CompareTag("Box") == true) {
            col.gameObject.GetComponent<MultiPlayerBox>().Break();
        }
    }
}
