﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MultiPlayer_Bomb : NetworkBehaviour {

    public float bombTimer;
    public float flameTimer;
    public int flamePower;

    private GameObject player;
    private Animator anim;

    // Set the spawner of the bomb
    public GameObject Spawner
    {
        set { player = value; }
        get { return player; }
    }

    public int Power
    {
        set { flamePower = value; }
        get { return flamePower; }
    }

    //private MultiPlayer_Player player;

    private MultiPlayer_BombManager manager;

	// Use this for initialization
	void Start ()
    {
        anim = gameObject.GetComponent<Animator>();


        // switch on boxCollider2D trigger
        gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

        manager = transform.GetComponent<MultiPlayer_BombManager>();
        manager.Set(transform);
        StartCoroutine(CountDown());

    }

    // When player touches the bomb, player won't be able to plant bomb
    // Fix issue of placing multiple bomb getting placed on the same tile
    void OnTriggerEnter2D(Collider2D col)
    {
        if (!isServer)
            return;

        if (col.CompareTag("Player") == true) {
            col.gameObject.GetComponent<MultiPlayer_Player>().canPlant = false;
        }
    }

    // When player exit the bomb, player can plant the bomb again
    void OnTriggerExit2D(Collider2D col)
    {
        if (!isServer)
            return;

        if (col.CompareTag("Player") == true) {

            col.gameObject.GetComponent<MultiPlayer_Player>().canPlant = true;

            // switch off trigger so it blocks player
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }

    public IEnumerator CountDown()
    {
        anim.SetFloat("Timer", bombTimer);

        yield return new WaitForSeconds(bombTimer);
        Explode();
    }

    private void Explode()
    {
        if (!isServer)
            return;

        Destroy(gameObject);
        Flame();
        player.GetComponent<MultiPlayer_Player>().RpcRecoverBomb();
    }

    private void Flame()
    {
        // middle 
        manager.CollisionDetector();
        manager.MakeMiddleFlame(transform.position);
        //
        //Each conditional handles the flame creation for the corresponding direction

        // top
        if (!manager.HasTopObject())
        {
            manager.MakeFlame(flamePower, 0, 1, manager.flame_2way_ver, manager.flame_tip_top);
        }

        // bottom
        if (!manager.HasBottomObject())
        {
            manager.MakeFlame(flamePower, 0, -1, manager.flame_2way_ver, manager.flame_tip_bot);
        }

        // right
        if (!manager.HasRightObject())
        {
            manager.MakeFlame(flamePower, 1, 0, manager.flame_2way_hor, manager.flame_tip_right);
        }

        // left
        if (!manager.HasLeftObject())
        {
            manager.MakeFlame(flamePower, -1, 0, manager.flame_2way_hor, manager.flame_tip_left);
        }  

    }
}
