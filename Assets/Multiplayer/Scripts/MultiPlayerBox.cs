﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MultiPlayerBox : NetworkBehaviour {


    public GameObject[] itemPrefabArray = new GameObject[3];

    public void Break()
    {
        if (!isServer)
            return;
        Debug.Log("breaking...");
        Destroy(gameObject);

        int item_index = Random.Range(0, itemPrefabArray.Length);
        var item = (GameObject)Instantiate(itemPrefabArray[item_index], transform.position, Quaternion.identity);
        NetworkServer.Spawn(item);

    }
}
