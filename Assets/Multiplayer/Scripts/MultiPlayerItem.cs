﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class MultiPlayerItem : NetworkBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!isServer)
            return;

        // Every item gets this script
        // so let item decide what tag they are
        if (col.CompareTag("Player") == true) {
            switch (gameObject.tag) {
                case "Potion_b": // bomb flame power up
                    col.gameObject.GetComponent<MultiPlayer_Player>().RpcBombPowerUp();
                    Debug.Log("potion object : potion_blue");
                    break;
                case "Potion_r": // player speed up 
                    col.gameObject.GetComponent<MultiPlayer_Player>().RpcSpeedUp();
                    Debug.Log("potion object : potion_red");
                    break;
                case "Potion_y": // bomb total Up
                    col.gameObject.GetComponent<MultiPlayer_Player>().RpcAddBomb();
                    Debug.Log("potion object : potion_yellow");
                    break;
            }
        }

        Destroy(gameObject);
        //Debug.Log("potion gone");
    }
}
