﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class MultiPlayer_Player : NetworkBehaviour {

    [SyncVar]
	public float speed;
    [SyncVar]
    public int bombTotal;
    [SyncVar]
    private bool isDead = false;
    [SyncVar]
    public int curHealth;
    [SyncVar]
    public int flamePower;
    [SyncVar]
    public int bombLeft;
    public int maxHealth = 100;
    public float maxSpeed = 7;
    public bool canPlant = true;

    private bool faceRight = false;
	private bool faceLeft = false;
	private bool faceUp = false;
	private bool faceDown = true;
    private bool walking = false;

    //
    public KeyCode moveUp;
    public KeyCode moveDown;
    public KeyCode moveRight;
    public KeyCode moveLeft;
    public KeyCode plantBomb;

	private Rigidbody2D rb2d;
	private Animator anim;
    private Vector3 position;

    public GameObject bombPrefab;
    public GameObject tombPrefab;
    public Canvas PlayerControlCanvasPrefab;
    public Canvas PlayerControlCanvas;


	private bool MoveUp = false;
    private bool MoveDown = false;
    private bool MoveLeft = false;
    private bool MoveRight = false;
    private bool Action = false;


	// Use this for initialization
	public void Start()
    {
        
		rb2d = gameObject.GetComponent<Rigidbody2D>();
		anim = gameObject.GetComponent<Animator>();

        bombLeft = bombTotal;
        curHealth = maxHealth;
        rb2d.gravityScale = 0;

        position = transform.position;

        // assign Animator parameters to NetworkAnimator
        // not necessary, but it stops NetworkAnimator error popping out while running in unity 
      /*  for (int i = 0; i < GetComponent<Animator>().parameterCount; i++)
            GetComponent<NetworkAnimator>().SetParameterAutoSend(i, true);*/

    }

    
    public override void OnStartLocalPlayer()
    {
        // use this function to do things like create camera, audio listeners, etc.
        // so things which has to be done only for our player

        if (PlayerControlCanvas == null)
        {
            PlayerControlCanvas = (Canvas)Instantiate(PlayerControlCanvasPrefab, Vector3.zero, Quaternion.identity);
            PlayerControlCanvas.sortingOrder = 1;
        }

        // Set up button hook
        var hook = PlayerControlCanvas.GetComponent<PlayerTouchControlHook>();
        // Set position of the control buttons 
        hook.panelPos.localPosition = Vector3.zero;

        // delegate pointer event to the hook
        hook.OnDownMoveDown = OnDownMoveDown;
        hook.OnDownMoveUp = OnDownMoveUp;
        hook.OnDownMoveRight = OnDownMoveRight;
        hook.OnDownMoveLeft = OnDownMoveLeft;
        hook.OnDownAction = OnDownAction;
        hook.OnUpMoveDown = OnUpMoveDown;
        hook.OnUpMoveUp = OnUpMoveUp;
        hook.OnUpMoveRight = OnUpMoveRight;
        hook.OnUpMoveLeft = OnUpMoveLeft;
        hook.OnUpAction = OnUpAction;

        // activate the buttons
        hook.SetLocalPlayer();

    }
    
    // Event from UI control
    public void OnDownMoveUp() {
		MoveUp = true;
	}

    public void OnDownMoveDown() {
		MoveDown = true;
	}

    public void OnDownMoveLeft() {
		MoveLeft = true;
	}
    
    public void OnDownMoveRight() {
		MoveRight = true;
	}

    public void OnDownAction() {
		Action = true;
	}

    public void OnUpMoveUp() {
		MoveUp = false;
	}

    public void OnUpMoveDown() {
        MoveDown = false;
	}

    public void OnUpMoveLeft() {
		MoveLeft = false;
	}

    public void OnUpMoveRight() {
		MoveRight = false;
	}

    public void OnUpAction() {
		Action = false;
	}

    // Player control behaviour
    // similar to single player, only PlantBomb and other stat buff is different
    
	void Update()
    {

        if (!isLocalPlayer)
            return;

        if (isDead)
            return;

        anim.SetBool("FaceRight", faceRight);
		anim.SetBool("FaceUp", faceUp);
		anim.SetBool("FaceDown", faceDown);
		anim.SetBool("FaceLeft", faceLeft);
        anim.SetBool("Walk", walking);

        if (transform.position == position)
        {
            walking = false;
            if (MoveRight == true || Input.GetKey(moveRight))
            {
                position += Vector3.right;
                walking = true;
                faceRight = true;
                faceLeft = faceUp = faceDown = false;
                if (!Walkable(position)) {
                    position = transform.position;
                }
                    
            }
            if (MoveLeft == true || Input.GetKey(moveLeft))
            {
                position += Vector3.left;
                walking = true;
                faceLeft = true;
                faceRight = faceUp = faceDown = false;
                if (!Walkable(position)) {
                    position = transform.position;
                }
            }
            if (MoveUp == true || Input.GetKey(moveUp))
            {
                position += Vector3.up;
                walking = true;
                faceUp = true;
                faceRight = faceLeft = faceDown = false;
                if (!Walkable(position)) {
                    position = transform.position;
                }
            }
            if (MoveDown == true || Input.GetKey(moveDown))
            {
                position += Vector3.down;
                walking = true;
                faceDown = true;
                faceRight = faceLeft = faceUp = false;
                if (!Walkable(position)) {
                    position = transform.position;
                }
            }
        }
        
        if ((Input.GetKey(plantBomb) && bombLeft > 0 || Action && bombLeft > 0) && canPlant)
        {
            bombLeft -= 1;
            CmdPlantBomb();
            
        }

        transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * speed);
    }


    [Command]
    void CmdPlantBomb()
    {
        GameObject bomb = Instantiate(bombPrefab, transform.position, Quaternion.identity) as GameObject;

        MultiPlayer_Bomb bombScript = bomb.GetComponent<MultiPlayer_Bomb>();

        bombScript.Spawner = gameObject;
        bombScript.Power = flamePower;
        // Get the server to spawn the bomb object, so other player can see it
        NetworkServer.Spawn(bomb);
        
    }


    [ClientRpc]
    public void RpcRecoverBomb()
    {
        // player recovers bomb
        if (bombLeft < bombTotal)
            bombLeft += 1;
    }

    public bool Walkable(Vector3 attempt)
    {
        // If detect game object, don't move
        var target = Physics2D.OverlapPoint(new Vector2(attempt.x, attempt.y));

        if (target != null && 
            target.CompareTag("Brick") ||
            target.CompareTag("Box") ||
            target.CompareTag("Tomb") ||
            target.CompareTag("Player") ||
            target.CompareTag("Tomb") ||
            target.CompareTag("Bomb")) {
            return false;
        }
        else {
            return true;
        }
    }


    public void TakeDamage(int dmg)
    {
        // Only server can call to damage the player
        if (!isServer)
            return;

        curHealth -= dmg;

        if (curHealth <= 0)
        {
            // kill the player
            RpcKillPlayer();
            CreateDeath();
        }
           
    }

    public override void OnNetworkDestroy()
    {
        // when the player object is destroy
    }

    [ClientRpc]
    public void RpcKillPlayer()
    {
        // kill player by assigning boolean value that it is dead
        // destroy the sprite so it won't get render
        isDead = true;
        Destroy(gameObject.GetComponent<SpriteRenderer>());
    }

 
    public void CreateDeath()
    {
        // Create the tomb on the player side
        GameObject tombstone = Instantiate(tombPrefab, transform.position, Quaternion.identity) as GameObject;
        
        // Spawn the tomb on server, so everybody sees it
        NetworkServer.Spawn(tombstone);
    }

    [ClientRpc]
    public void RpcBombPowerUp()
    {
        // increase the power of the bomb
        flamePower += 1;
    }

    [ClientRpc]
    public void RpcSpeedUp()
    {
        // increase the movement speed
        if (speed < maxSpeed)
            speed += 0.5f;
    }

    [ClientRpc]
    public void RpcAddBomb()
    {
        // increase total number of bomb
        bombTotal += 1;
        bombLeft += 1;
    }
}
