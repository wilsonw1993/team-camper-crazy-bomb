﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class MultiplayerMenu : MonoBehaviour
{

    private int multiplayerIndex = 1;
    private int mainMenuIndex = 0;

    public GameObject LobbyManager;


    public void MultiplayerButton()
    {
        //load the multiplayer scene
        Application.LoadLevel(multiplayerIndex);
    }

    public void MainMenuButton()
    {
        //destroy the networking
        Destroy(LobbyManager);

        //load the main menu scene
        Application.LoadLevel(mainMenuIndex);
    }

}

