#**Crazy Bomb**#
A multiplayer action game created for a university project. It is a one shot kill bomb planting game that is integrated with Google Play Services. Crazy Bomb also has a single player mode and was built to be use on Android only. 

#**Installation**#
Clone the project, open in Unity 5.2.x and build out to an apk.

#**Instructions to run/test**#

* The intended initial landing page of the game is in /Assets/Main Menu. Run Main Menu scene to see. 

* All Multiplayer functionality is handled in /Assets/Multiplayer.

* Google Play Service functionality is implemented in /Assets/Multiplayer/Scripts/GPSManager.cs 

* Most UI is implemented with the help of Game Jam Template (/Assets/Game Jam Menu Template). 

* Single Player is implemented in the main branch, /Assets/Scenes & /Assets/Scripts 

#**Main External Sources**#
[Game Jam Template](https://www.assetstore.unity3d.com/en/#!/content/40465)

[Google Play Services Plugin for Unity](https://github.com/playgameservices/play-games-plugin-for-unity)

#**Authors**#
Created by: Brandon, Jone, Leah, Wilson


----

Any copyright was unintentional and was not for commercial use.